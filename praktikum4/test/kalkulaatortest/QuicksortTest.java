package kalkulaatortest;

import static org.junit.Assert.*;

import java.util.Arrays;

import kalkulaator.Quicksort;

import org.junit.Test;

public class QuicksortTest {

	@Test
	public void test1() {
		int[] list = Quicksort.quicksort(new int[]{2}, 0, 0);
		int[] expected = {2};
		assertTrue("Result is different then expected", Arrays.equals(expected, list));
	}
	
	@Test
	public void test2() {
		int[] list = Quicksort.quicksort(new int[]{2, 1}, 0, 1);
		int[] expected = {1, 2};
		assertTrue("Result is different then expected", Arrays.equals(expected, list));
	}
	
	@Test
	public void test3() {
		int[] list = Quicksort.quicksort(new int[]{4, 2, 0}, 0, 2);
		int[] expected = {0, 2, 4};
		assertTrue("Result is different then expected", Arrays.equals(expected, list));
	}
	
	@Test
	public void test4() {
		int[] list = Quicksort.quicksort(new int[]{22, 1, 2, 99, 123, 47, 55}, 0, 6);
		int[] expected = {1, 2, 22, 47, 55, 99, 123};
		assertTrue("Result is different then expected", Arrays.equals(expected, list));
	}
}
