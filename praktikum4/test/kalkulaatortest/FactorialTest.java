package kalkulaatortest;

import static org.junit.Assert.*;
import kalkulaator.Factorial;

import org.junit.Test;

public class FactorialTest {

	@Test
	public void test1() {
		assertEquals("Result is different then expected", 1, Factorial.factorial(1));
	}
	
	@Test
	public void test2() {
		assertEquals("Result is different then expected", 2, Factorial.factorial(2));
	}
	
	@Test
	public void test3() {
		assertEquals("Result is different then expected", 6, Factorial.factorial(3));
	}
}
