package kalkulaatortest;

import static org.junit.Assert.*;
import kalkulaator.PositivePairSum;

import org.junit.Test;

public class PositivePairSumTest {

	@Test
	public void test1() {
		assertEquals("Result is different then expected", 2, 
				PositivePairSum.positivePairSum(new int[]{2}));
	}
	
	@Test
	public void test2() {
		assertEquals("Result is different then expected", 12, 
				PositivePairSum.positivePairSum(new int[]{2, 4, 6}));
	}
	
	@Test
	public void test3() {
		assertEquals("Result is different then expected", 12, 
				PositivePairSum.positivePairSum(new int[]{-2, -4, 2, 4, 6}));
	}
	
	@Test
	public void test4() {
		assertEquals("Result is different then expected", 12, 
				PositivePairSum.positivePairSum(new int[]{1, 2, 4, 6}));
	}
	
	@Test
	public void test5() {
		assertEquals("Result is different then expected", 12, 
				PositivePairSum.positivePairSum(new int[]{-2, -4, 1, 2, 4, 6}));
	}
}
