package kalkulaatortest;

import static org.junit.Assert.*;
import kalkulaator.Sum;

import org.junit.Test;

public class SumTest {

	@Test
	public void test1() throws Exception {
		assertEquals("Result is different then expected", 5, Sum.sum("2,3"));
	}
	
	@Test (expected = Exception.class)
	public void test2() throws Exception {
		assertEquals("Result is different then expected", 1, Sum.sum("1,x"));
	}
	
	@Test
	public void test3() throws Exception {
		assertEquals("Result is different then expected", 3, Sum.sum("\"\",3"));
	}
	
	@Test
	public void test4() throws Exception {
		assertEquals("Result is different then expected", 5, Sum.sum( " \"\",3, 2 "));
	}
	
	@Test
	public void test5() throws Exception {
		assertEquals("Result is different then expected", 10, Sum.sum("1,2,3,4"));
	}
	
	@Test
	public void test6() throws Exception {
		assertEquals("Result is different then expected", 5, Sum.sum("2;3"));
	}
	
	@Test (expected = Exception.class)
	public void test7() throws Exception {
		assertEquals("Result is different then expected", -1, Sum.sum("2,-3"));
	}
}
