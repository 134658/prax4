package kalkulaator;

public class Factorial {
	
	public static int factorial(int value) {
		if (value == 1) return 1;
		else if (value == 2) return 2;
		else {
			int factorial = value;
			for (int i = value - 1; i > 0; i--) {
				factorial = factorial * i;
			}
			return factorial;
		}
	}
}
