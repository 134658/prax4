package kalkulaator;

public class PositivePairSum {

	public static int positivePairSum(int[] values) {
		int sum = 0;
		if (values.length == 0) return sum;
		for (int i: values) {
			if (i > 0) {
				if (i % 2 == 0) {
					sum += i;
				}
			}
		}
		return sum;
	}
}
