package kalkulaator;

public class Quicksort {
	
    //Hoare quicksort
	public static int[] quicksort(int[] values, int low, int high) {
		if (low < high) {
			int p = partition(values, low, high);
			quicksort(values, low, p);
			quicksort(values, p + 1, high);
		}
		return values;
	}
	
	private static int partition(int[] values, int low, int high) {
		int pivot = values[low];
		int i = low - 1;
		int j = high + 1;
		while (true) {
			do {
				j--;
			}	while (values[j] > pivot);
			do {
				i++;
			}	while (values[i] < pivot);
			
			if (i < j) {
				swap(values, i, j);
			} else {
				return j;
			}
		}
	}
	
	private static void swap(int[] values, int i, int j) {
		int tmp = values[i];
		values[i] = values[j];
		values[j] = tmp;
	}
}
