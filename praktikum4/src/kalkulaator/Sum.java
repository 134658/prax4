package kalkulaator;

public class Sum {

	public static int sum(String line) throws Exception {
		int sum = 0;
		String[] split = line.replaceAll("\\s+|\"","").split(",|;");
		for (String s: split) {
			if (s.equals("")) continue;
			if (isInteger(s)) {
				if (Integer.valueOf(s) >= 0) {
					sum += Integer.valueOf(s);
				} else {
					throw new Exception("Negative number");
				}
			} else {
				throw new Exception("Invalid syntax");
			}
		}
		return sum;
	}
	
	private static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
